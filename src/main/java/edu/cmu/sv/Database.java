package edu.cmu.sv;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.openrdf.sail.memory.MemoryStore;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by David Cohen on 6/21/14.
 */
public class Database {

    // a counter used to create new URIs
    public final RepositoryConnection connection;
    public final static String baseURI = "http://sv.cmu.edu/sparql_playground#";
    public final static String prefixes = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
            "PREFIX base: <"+baseURI+">\n";

    public void log(String interaction){
//        System.out.println(interaction);
    }

    public void loadTurtleFile(String filename){
        try {
            connection.add(new InputStreamReader(new FileInputStream(filename), "UTF-8"),
                    baseURI, RDFFormat.TURTLE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RDFParseException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public Database() {
        // inferencing rdf database
        Repository repository = new SailRepository(new ForwardChainingRDFSInferencer(new MemoryStore()));
        Object tmpConnection = null;
        try {
            repository.initialize();
            tmpConnection = repository.getConnection();
        } catch (RepositoryException e) {
            System.exit(0);
        }
        connection = (RepositoryConnection) tmpConnection;
    }

    /*
    * Run a sparql query on the database and return all values for the variable x
    * */
    public Set<String> runQuerySelectX(String queryString){
        log(queryString);
        Set<String> ans = new HashSet<>();
        try {
            synchronized (connection) {
                TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
                TupleQueryResult result = query.evaluate();

                while (result.hasNext()) {
                    BindingSet bindings = result.next();
                    ans.add(bindings.getValue("x").stringValue());
                }
                result.close();
            }
        } catch (RepositoryException | QueryEvaluationException | MalformedQueryException e) {
            e.printStackTrace();
            System.exit(0);
        }
        return ans;
    }

    /*
* Run a sparql query on the database and return all value pairs for the variables x and y
* */
    public Set<Pair<String, String>> runQuerySelectXAndY(String queryString){
        log(queryString);
        Set<Pair<String, String>> ans = new HashSet<>();
        try {
            synchronized (connection) {
                TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
                TupleQueryResult result = query.evaluate();

                while (result.hasNext()) {
                    BindingSet bindings = result.next();
                    ans.add(new ImmutablePair<>(bindings.getValue("x").stringValue(),
                            bindings.getValue("y").stringValue()));
                }
                result.close();
            }
        } catch (RepositoryException | QueryEvaluationException | MalformedQueryException e) {
            e.printStackTrace();
        }
        return ans;
    }

}
