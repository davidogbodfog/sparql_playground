package edu.cmu.sv;

public class App
{
    public static void main( String[] args )
    {
        String prefixes = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
                "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n" +
                "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" +
                "@prefix base: <http://sv.cmu.edu/yoda#> .";

        Database d = new Database();
//        d.loadTurtleFile("./src/resources/test.turtle");
        d.loadTurtleFile("./src/resources/anushka_test.turtle");
        System.out.println("loaded anushka test");

//        {
//            String queryString = Database.prefixes;
//            queryString += "SELECT ?x ?y WHERE {BIND(NOW() AS ?x) . BIND(TIMEZONE(?x) AS ?y)}";
//            System.out.println(d.runQuerySelectXAndY(queryString));
//        }
//
//        {
//            String queryString = Database.prefixes;
//            queryString += "SELECT ?x WHERE {\n";
//            queryString += "?x rdf:type base:Person";
//            queryString += "}";
//            Set<String> results = d.runQuerySelectX(queryString);
//            System.out.println(results);
//        }
//
        {
            String queryString = Database.prefixes;
            queryString += "SELECT ?x WHERE {\n";
            queryString += "?y rdf:type <http://sv.cmu.edu/yoda#Instructor> . ";
            queryString += "?y rdfs:label ?x . ";
            queryString += "}";
            System.out.println(d.runQuerySelectX(queryString));
        }
//
//
//        {
//            String queryString = Database.prefixes;
//            queryString += "SELECT ?x ?score WHERE {\n";
//            queryString += "{\n";
//            queryString += "?x base:dum base:0001 .\n";
//            queryString += "BIND (1.0 AS ?score) \n";
//            queryString += "} UNION {\n";
//            queryString += "?x rdf:type ?y .";
//            queryString += "FILTER NOT EXISTS {?x base:dum base:0001 }\n";
//            queryString += "BIND (0.0 AS ?score) } \n";
//            queryString += "}";
////            queryString += "?x rdf:type base:Person";
////            queryString += "}";
//
//            List<String> ans = new LinkedList<>();
//            try {
//                TupleQuery query = d.connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString, Database.baseURI);
//                TupleQueryResult result = query.evaluate();
//
//                while (result.hasNext()){
//                    BindingSet bindings = result.next();
//                    ans.add(bindings.getValue("x").stringValue() + " -> " + bindings.getValue("score").stringValue());
//                }
//                result.close();
//            } catch (RepositoryException | QueryEvaluationException | MalformedQueryException e) {
//                e.printStackTrace();
//                System.exit(0);
//            }
//            System.out.println(ans);
//        }




    }
}
